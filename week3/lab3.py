import random

# with open("data1.txt", "w") as file:
#     for number in range(101):
#         file.write(str(number) + "\n")
#
# print("saved")

#
# with open("myFiles2.txt", "w") as file:
#     for i in range(1, 31):
#         file_name = f"Programmer{i}.txt"
#         file.write(file_name + "\n")
#
# print("Saved")
# Function values in [0; 2] range and writing to "function.txt" and "myFiles" folder
#
# import random
#
# function_values = [random.uniform(0, 2) for _ in range(10)]
#
# with open("function.txt", "w") as file:
#     for value in function_values:
#         file.write(str(value) + "\n")
#
# for i, value in enumerate(function_values, start=1):
#     file_name = f"function_{i}.txt"
#     file_path = "myFiles/" + file_name
#
# print("saved")
#
# my_dict = {0: 10, 1: 20}
# print("დაწერილი ლექსიკონი:", my_dict)
# my_dict.update({2: 30, 3: 40})
# print("დამატებული ელემენტები:", my_dict)
# if 1 in my_dict:
#     del my_dict[1]
# print("წაშლილი ელემენტი:", my_dict)

# d = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
# key_to_check = 3
# if key_to_check in d:
#     print(f"Key {key_to_check} არის ლექსიკონში, მისი მნიშვნელობა არის {d[key_to_check]}.")
# else:
#     print(f"Key {key_to_check} არ არის ლექსიკონში.")
#
# my_list = [0, 1, 2, 3, 4]
# my_list.extend([5, 6, 7])
# my_list.pop(2)
# my_list.pop(4)
# for item in my_list:
#     print(item)
# count = len(my_list)
# print(f"სიმრავლეში არის {count} ელემენტი.")

my_tuple = tuple(x**3 for x in range(1, 101) if x % 5 == 0)
print(my_tuple)
tuple_length = len(my_tuple)
print(f"Tuple-ის სიგრძე არის {tuple_length}.")
